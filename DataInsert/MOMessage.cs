﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;


namespace MsgBox
{
    public class MOMessage
    {
        public SqlConnection _con;
        public SqlCommand Cmd;
        private SqlDataAdapter _da;
        private DataTable _dt;
         

        public void SqlQuery(string queryText)
        {
            Cmd = new SqlCommand(queryText,_con);
        }

        public DataTable QueryEx()
        {
            _da = new SqlDataAdapter(Cmd);
            _dt = new DataTable();
            _da.Fill(_dt);
            return _dt;
        }

        public void NonQueryEx()
        {
            Cmd.ExecuteNonQuery();
        }

        public DataSet getMSG(BRProp BrObject, COMessage CoObject)
        {
            ////this will get the whitelist from database base on Listtype  
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(CoObject.strConString);

            using (System.Data.SqlClient.SqlCommand slqcommands = new System.Data.SqlClient.SqlCommand("sprGetMessageBox", con))
            {
                slqcommands.Parameters.AddWithValue("@MsgType", BrObject.msgType);
                slqcommands.Parameters.AddWithValue("@DisplayType", BrObject.displayType);
                slqcommands.CommandTimeout = CoObject.SQLTimeout;
                slqcommands.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adp = new SqlDataAdapter(slqcommands);
                adp.Fill(ds);
            }

            return ds;
        }
    }
}
