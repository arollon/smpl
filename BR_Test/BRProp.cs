﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MsgBox
{
    public class BRProp
    {

        #region Request 
        public int msgType { get; set; }
        public string headerMessage { get; set; }
        public string bodyMsg { get; set; }
        public string displayType { get; set; }
        public string code { get; set; }
        public string status { get; set; }
        public string customMsg { get; set; }

        #endregion

        #region Return
        public string icon { get; set; }
        public int button { get; set; }
        public string Message { get; set; }
        

        #endregion
    }
}
