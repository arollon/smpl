﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace MsgBox
{
    public class BRMessage
    {
        BRProp BrProp = new BRProp();
        BRProp BrRET = new BRProp();
        BRProp BrMSG = new BRProp();
        MOMessage MoMsg = new MOMessage();
        COMessage CoMsg = new COMessage();
        


        public BRProp getMsg(BRProp brObject, string displayType)
        {
            StreamWriter filetxt;

            if (!File.Exists("logfile.txt"))
            {
                filetxt = new StreamWriter("logfile.txt");
            }
            else
            {
                filetxt = new StreamWriter("logfile.txt", true);
            }
            
            var now = DateTime.Now;
            var time = now.ToString("hh:mm:ss tt");
            var date = now.ToString("MM/dd/yy");
            try
            {
            DataSet dsRet = new DataSet();
             brObject.displayType = displayType;
            dsRet = MoMsg.getMSG(brObject, CoMsg);
                ///put datatable checker 
                ///
            if (dsRet.Tables.Count > 0)
            {
                if (dsRet.Tables[0].Rows.Count > 0)
             {
                     brObject.headerMessage = dsRet.Tables[0].Rows[0]["HMessage"].ToString();
                     brObject.bodyMsg = dsRet.Tables[1].Rows[0]["Message"].ToString();
                     brObject.code = dsRet.Tables[2].Rows[0]["DisplayType"].ToString();
             }
            }
                filetxt.Write(date + " " + time + " getMSG " + brObject.code + " '" + brObject.bodyMsg + "'" + Environment.NewLine);
                filetxt.Close();
                return brObject;
                
            }  

            catch (Exception e)
            {

                brObject.status = "DLL ERROR: " + e.Message + ".";
                filetxt.Write(date + " " + time + " getMSG " + "'" + brObject.status + "'" + Environment.NewLine);
                filetxt.Close();
                return brObject;
                
            }
            
            
            
        }


    }
}
