﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;
using System.ComponentModel;

namespace MsgBox
{
    public class COMessage
    {
        RWINI RWini = new RWINI(System.AppDomain.CurrentDomain.BaseDirectory.ToString() + @"\MessageBox.ini");
        public DataTable Dtable = new DataTable();
        public string strConString;// RWini.Read("DB","ConnectionString");
        public int SQLTimeout;

        public  COMessage()
        {
            strConString = RWini.Read("DB", "ConnectionString");
            SQLTimeout = Int32.Parse(RWini.Read("DB", "SQLTimeout"));
        }
    }
}
